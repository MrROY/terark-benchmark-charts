/**
 * Created by royguo on 4/4/16 AD.
 */

var Sequelize = require("sequelize")

var db = require("./db")

var EngineLatency = db.define('engine_test_latency_10s', {
    time_bucket: Sequelize.INTEGER,
    latency_us: Sequelize.INTEGER,
    op_type: Sequelize.INTEGER,
    cnt: Sequelize.INTEGER,
    engine_name: Sequelize.TEXT
}, {
    timestamps: false,
    tableName: "engine_test_latency_10s"
});


EngineLatency.findAllByName = function (name, time_bucket) {
    // console.log("time_bucket = " + time_bucket)

    return EngineLatency.findAll({
        attributes: ["time_bucket", "latency_us", "op_type", "cnt"],
        order: [["time_bucket", "ASC"]],
        where: {
            engine_name: name,
            time_bucket: {
                $in: time_bucket
            }
        },
        raw: true
    }).then(function (data) {
        var result = {
            time_bucket: [],

            read_total: [],
            read_100ms: [],
            read_15ms: [],
            read_5ms: [],
            read_2ms: [],
            read_500us: [],

            insert_total: [],
            insert_100ms: [],
            insert_15ms: [],
            insert_5ms: [],
            insert_2ms: [],
            insert_500us: []
        }

        var length = -1
        data.reduce(function (previousItem, currentItem, index, array) {
            // 每当遇到新的时间bucket，所有数组增长一位
            if (previousItem == 0 || currentItem.time_bucket != previousItem.time_bucket) {
                length += 1
                result.time_bucket[length] = currentItem.time_bucket

                result.read_total[length] = 0
                result.read_100ms[length] = 0
                result.read_15ms[length] = 0
                result.read_5ms[length] = 0
                result.read_2ms[length] = 0
                result.read_500us[length] = 0

                result.insert_total[length] = 0
                result.insert_100ms[length] = 0
                result.insert_15ms[length] = 0
                result.insert_5ms[length] = 0
                result.insert_2ms[length] = 0
                result.insert_500us[length] = 0
            }

            switch (currentItem.op_type) {
                case 1:
                    result.read_total[length] += currentItem.cnt
                    if (currentItem.latency_us <= 100 * 1000) {
                        result.read_100ms[length] += currentItem.cnt
                    }
                    if (currentItem.latency_us <= 15 * 1000) {
                        result.read_15ms[length] += currentItem.cnt
                    }
                    if (currentItem.latency_us <= 5 * 1000) {
                        result.read_5ms[length] += currentItem.cnt
                    }
                    if (currentItem.latency_us <= 2 * 1000) {
                        result.read_2ms[length] += currentItem.cnt
                    }
                    if (currentItem.latency_us <= 500 ) {
                        result.read_500us[length] += currentItem.cnt
                    }
                    break
                case 2:
                    result.insert_total[length] += currentItem.cnt
                    if (currentItem.latency_us <= 100 * 1000) {
                        result.insert_100ms[length] += currentItem.cnt
                    }
                    if (currentItem.latency_us <= 15 * 1000) {
                        result.insert_15ms[length] += currentItem.cnt
                    }
                    if (currentItem.latency_us <= 5 * 1000) {
                        result.insert_5ms[length] += currentItem.cnt
                    }
                    if (currentItem.latency_us <= 2 * 1000) {
                        result.insert_2ms[length] += currentItem.cnt
                    }
                    if (currentItem.latency_us <= 500 ) {
                        result.insert_500us[length] += currentItem.cnt
                    }
                    break
            }
            return currentItem
        }, 0)

        for (var i = 0; i < result.time_bucket.length; i++) {
            result.read_100ms[i] = (result.read_100ms[i] *100 / result.read_total[i]).toFixed(4)
            result.read_15ms[i] = (result.read_15ms[i] * 100 / result.read_total[i]).toFixed(4)
            result.read_5ms[i] = (result.read_5ms[i] * 100 / result.read_total[i]).toFixed(4)
            result.read_2ms[i] = (result.read_2ms[i] * 100 / result.read_total[i]).toFixed(4)
            result.read_500us[i] = (result.read_500us[i] * 100 / result.read_total[i]).toFixed(4)

            result.insert_100ms[i] = (result.insert_100ms[i] * 100 / result.insert_total[i]).toFixed(4)
            result.insert_15ms[i] = (result.insert_15ms[i] * 100 / result.insert_total[i]).toFixed(4)
            result.insert_5ms[i] = (result.insert_5ms[i] * 100 / result.insert_total[i]).toFixed(4)
            result.insert_2ms[i] = (result.insert_2ms[i] * 100 / result.insert_total[i]).toFixed(4)
            result.insert_500us[i] = (result.insert_500us[i] * 100 / result.insert_total[i]).toFixed(4)

            // if(result.insert_100ms[i] == '75.0000') {
            //     console.log(result.time_bucket[i])
            //     console.log(result.read_total[i])
            //     console.log(result.insert_total[i])
            // }
        }

        return result
    })
}

module.exports = EngineLatency
