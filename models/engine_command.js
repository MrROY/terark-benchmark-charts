/**
 * Created by royguo on 4/4/16 AD.
 */

var Sequelize = require("sequelize")

var db = require("./db")

var EngineCommand = db.define('engine_test_command', {
    env: Sequelize.TEXT,
    command_line: Sequelize.TEXT,
    time: Sequelize.INTEGER,
    engine_name: Sequelize.TEXT
}, {
    timestamps: false,
    tableName: "engine_test_command"
});


EngineCommand.findAllByName = function (name) {
    return EngineCommand.findAll({
        attributes: ["time", "command_line", "env"],
        order: [["time", "DESC"]],
        where: {
            engine_name: name
        },
        raw: true
    }).then(function (data) {
        return data
    })
}

module.exports = EngineCommand
