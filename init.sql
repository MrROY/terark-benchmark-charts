﻿# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: rds432w5u5d17qd62iq3o.mysql.rds.aliyuncs.com (MySQL 5.6.29)
# Database: benchmark
# Generation Time: 2017-09-04 01:54:34 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE benchmark;
CREATE DATABASE benchmark;


# Dump of table engine_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_config`;

CREATE TABLE `engine_config` (
  `name` varchar(32) NOT NULL,
  `config` text NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table engine_test_command
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_test_command`;

CREATE TABLE `engine_test_command` (
  `engine_name` varchar(50) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL,
  `command_line` text NOT NULL,
  `env` text NOT NULL,
  KEY `engine_name` (`engine_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table engine_test_cpu_10s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_test_cpu_10s`;

CREATE TABLE `engine_test_cpu_10s` (
  `time_bucket` int(11) NOT NULL,
  `iowait` double(8,0) NOT NULL,
  `usage` double(8,2) NOT NULL,
  `engine_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`time_bucket`,`engine_name`),
  KEY `timebucketindex` (`time_bucket`) USING BTREE,
  KEY `enginenameindex` (`engine_name`) USING HASH,
  KEY `engine_timebucket` (`engine_name`,`time_bucket`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table engine_test_dbsize_10s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_test_dbsize_10s`;

CREATE TABLE `engine_test_dbsize_10s` (
  `time_bucket` int(11) NOT NULL,
  `dbsize` int(11) NOT NULL,
  `engine_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`time_bucket`,`engine_name`),
  KEY `engine_name` (`engine_name`) USING HASH,
  KEY `engine_name_timebucket` (`engine_name`,`time_bucket`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table engine_test_diskinfo_10s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_test_diskinfo_10s`;

CREATE TABLE `engine_test_diskinfo_10s` (
  `time_bucket` int(11) NOT NULL,
  `diskinfo` text,
  `engine_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`time_bucket`,`engine_name`),
  KEY `enginename` (`engine_name`) USING HASH,
  KEY `engine_timebucket` (`engine_name`,`time_bucket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table engine_test_latency_10s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_test_latency_10s`;

CREATE TABLE `engine_test_latency_10s` (
  `time_bucket` int(11) NOT NULL,
  `op_type` int(11) NOT NULL,
  `latency_us` int(11) NOT NULL,
  `cnt` int(11) NOT NULL,
  `engine_name` varchar(60) NOT NULL,
  PRIMARY KEY (`engine_name`,`time_bucket`,`op_type`,`latency_us`,`cnt`),
  KEY `engine_name` (`engine_name`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table engine_test_memory_10s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_test_memory_10s`;

CREATE TABLE `engine_test_memory_10s` (
  `time_bucket` int(11) NOT NULL,
  `free_memory` int(11) NOT NULL COMMENT 'MB',
  `cached_memory` int(11) NOT NULL COMMENT 'MB',
  `used_memory` int(11) NOT NULL COMMENT 'MB',
  `engine_name` varchar(50) NOT NULL DEFAULT '',
  `total_memory` int(11) NOT NULL COMMENT 'MB',
  PRIMARY KEY (`time_bucket`,`engine_name`),
  KEY `time_bucket` (`time_bucket`) USING BTREE,
  KEY `engine_name` (`engine_name`) USING BTREE,
  KEY `engine_timebucket` (`engine_name`,`time_bucket`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table engine_test_ops_10s
# ------------------------------------------------------------

DROP TABLE IF EXISTS `engine_test_ops_10s`;

CREATE TABLE `engine_test_ops_10s` (
  `time_bucket` int(11) NOT NULL,
  `ops` int(11) NOT NULL,
  `ops_type` int(11) NOT NULL,
  `engine_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`time_bucket`,`ops_type`,`engine_name`),
  KEY `engine_name` (`engine_name`) USING HASH,
  KEY `engine_timebucket` (`engine_name`,`time_bucket`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
