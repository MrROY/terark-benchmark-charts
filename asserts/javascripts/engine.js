function bindClickHistoryEvent() {
    $("#start-history .menu").click(function () {
        $("#history-menu .menu").css("background-color", "#EEE")
        $(this).css("background-color", "#CCC")

        $("#history-left").html($(this).attr("command"))
        $("#history-right").html($(this).attr("env"))
        $("#start-time").val($(this).attr("start"))
        $("#end-time").val($(this).attr("end"))
    })
}

function fetchCommandHistory(name) {
    // console.log("拉取引擎历史启动记录, name = " + name)
    $.ajax({
        type: "GET",
        url: "/api/engine/history/" + name,
        contentType: "application/json",
        async: true,
        dataType: "json",
        success: function (data) {
            // console.log(data)
            // list all history commands by this engine_name
            var html = ""
            $(data).each(function (index, cmd) {
                const start = new Date(cmd.time * 1000).Format("yyyy-MM-dd hh:mm:ss")
                const end = new Date((cmd.time + 2*86400) * 1000).Format("yyyy-MM-dd hh:mm:ss")
                html = html + "<span class='menu' "
                    + "start='" + start + "' end='" + end + "' "
                    + "command='"+cmd.command_line+"' env='"+cmd.env+"'>"
                    // + start + " ~ " + end
                    + start
                    + "</span>";
            })
            $("#history-menu").html(html)
            if(data.length == 0) {
                $("#history-left").html('No history data')
                $("#history-right").html('No history data')
            }else{
                $("#history-left").html('Please select a time period')
                $("#history-right").html('Please select a time period')
            }
            // bind click button events of these time periods.
            bindClickHistoryEvent()
        }
    });
}
// function loadStartHistory(engine, callback) {
//     $.ajax({
//         type: "POST",
//         url: "/api/engine/history",
//         contentType: "application/json",
//         async: true,
//         data: JSON.stringify({
//             "engine": engine
//         }),
//         dataType: "json",
//         success: function (data) {
//             callback(data)
//         }
//     });
// }