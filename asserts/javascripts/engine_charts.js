// 重新加载页面内的所有折线图
function reloadChart() {
    $("#loading").show()
    $(".chart").show()
    const engine = $(".engine.blue-button").attr("value")
    const step = $(".step.blue-button").attr("value")
    const duration = $("#start-time").val() + "~" + $("#end-time").val()

    const opsChart = echarts.init(document.getElementById('ops-chart'));
    const cpuChart = echarts.init(document.getElementById('cpu-chart'))
    const memoryChart = echarts.init(document.getElementById('memory-chart'))
    const dbsizeChart = echarts.init(document.getElementById('dbsize-chart'))
    const latencyChart = echarts.init(document.getElementById('latency-chart'))

    // 指定图表的配置项和数据
    var opsOption = {
        title: {
            text: 'OPS'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['Total', 'Read', 'Write', 'Update']
        },
        toolbox: {
            feature: {saveAsImage: {}}
        },
        yAxis: [
            {type: 'value'}
        ],
        dataZoom: [
            {
                type: 'slider',
                show: true,
                xAxisIndex: [0],
                start: 0,
                end: 100
            }
        ]
    };
    var latencyOption = {
        title: {
            text: 'Latency(Log)'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['Read_100ms', 'Read_15ms', 'Read_5ms', 'Read_2ms', 'Read_500us',
                   'Write_100ms', 'Write_15ms', 'Write_5ms', 'Write_2ms', 'Write_500us']
        },
        toolbox: {
            feature: {saveAsImage: {}}
        },
        yAxis: [
            {type: 'log'}
        ],
        dataZoom: [
            {
                type: 'slider',
                show: true,
                xAxisIndex: [0],
                start: 0,
                end: 100
            }
        ]
    };
    var cpuOption = {
        title: {
            text: 'CPU Usage'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['usage(%)', 'iowait(%)']
        },
        toolbox: {
            feature: {saveAsImage: {}}
        },
        yAxis: [
            {type: 'value'}
        ],
        dataZoom: [
            {
                type: 'slider',
                show: true,
                xAxisIndex: [0],
                start: 0,
                end: 100
            }
        ]
    };
    // 指定图表的配置项和数据
    var memoryOption = {
        title: {
            text: 'Memory Usage(MB)'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['Total', 'Free', 'Cached', 'Used']
        },
        toolbox: {
            feature: {saveAsImage: {}}
        },
        yAxis: [
            {type: 'value'}
        ],
        dataZoom: [
            {
                type: 'slider',
                show: true,
                xAxisIndex: [0],
                start: 0,
                end: 100
            }
        ]
    };

    // 指定图表的配置项和数据
    var dbsizeOption = {
        title: {
            text: 'DB Size(MB)'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['dbsize(MB)']
        },
        toolbox: {
            feature: {saveAsImage: {}}
        },
        yAxis: [
            {type: 'value'}
        ],
        dataZoom: [
            {
                type: 'slider',
                show: true,
                xAxisIndex: [0],
                start: 0,
                end: 100
            }
        ]
    };

    function makeRequest() {
        $.ajax({
            type: "POST",
            url: "/api/engine",
            contentType: "application/json",
            async: true,
            data: JSON.stringify({
                "engine": engine,
                "step": step,
                "duration": duration
            }),
            dataType: "json",
            success: function (data) {
                $("#loading").hide();
                // OPS
                opsOption.xAxis = {
                    data: data['time_bucket']
                }
                opsOption.series = [{
                    name: 'Total',
                    type: 'line',
                    data: data['ops_total']
                }, {
                    name: 'Read',
                    type: 'line',
                    data: data['ops_read'],
                    markLine: {
                        data: [
                            {type: 'average', name: 'Read Average'}
                        ]
                    }
                }, {
                    name: 'Write',
                    type: 'line',
                    data: data['ops_insert'],
                    markLine: {
                        data: [
                            {type: 'average', name: 'Write Average'}
                        ]
                    }
                }, {
                    name: 'Update',
                    type: 'line',
                    data: data['ops_update'],
                    markLine: {
                        data: [
                            {type: 'average', name: 'Update Average'}
                        ]
                    }
                }]
                opsChart.setOption(opsOption);

                // Latency
                latencyOption.xAxis = {
                    data: data['time_bucket']
                }
                latencyOption.series = [{
                    name: 'Read_100ms',
                    type: 'line',
                    data: data['latency_read_100ms']
                }, {
                    name: 'Read_15ms',
                    type: 'line',
                    data: data['latency_read_15ms']
                }, {
                    name: 'Read_5ms',
                    type: 'line',
                    data: data['latency_read_5ms']
                }, {
                    name: 'Read_2ms',
                    type: 'line',
                    data: data['latency_read_2ms']
                }, {
                    name: 'Read_500us',
                    type: 'line',
                    data: data['latency_read_500us']
                }, {
                    name: 'Write_100ms',
                    type: 'line',
                    data: data['latency_insert_100ms']
                }, {
                    name: 'Write_15ms',
                    type: 'line',
                    data: data['latency_insert_15ms']
                }, {
                    name: 'Write_5ms',
                    type: 'line',
                    data: data['latency_insert_5ms']
                }, {
                    name: 'Write_2ms',
                    type: 'line',
                    data: data['latency_insert_2ms']
                }, {
                    name: 'Write_500us',
                    type: 'line',
                    data: data['latency_insert_500us']
                }]
                latencyChart.setOption(latencyOption);

                // CPU Usage
                cpuOption.xAxis = {
                    data: data['time_bucket']
                }
                cpuOption.series = [{
                    name: 'usage(%)',
                    type: 'line',
                    data: data['cpu_usage']
                }, {
                    name: 'iowait(%)',
                    type: 'line',
                    data: data['cpu_iowait']
                }]
                cpuChart.setOption(cpuOption);

                // Memory Usage
                memoryOption.xAxis = {
                    data: data['time_bucket']
                }
                memoryOption.series = [{
                    name: 'Total',
                    type: 'line',
                    data: data['total_memory']
                }, {
                    name: 'Free',
                    type: 'line',
                    data: data['free_memory']
                }, {
                    name: 'Cached',
                    type: 'line',
                    data: data['cached_memory']
                }, {
                    name: 'Used',
                    type: 'line',
                    data: data['used_memory']
                }]
                memoryChart.setOption(memoryOption);

                // DB Size/ Disk Info
                dbsizeOption.xAxis = {
                    data: data['time_bucket']
                }
                dbsizeOption.tooltip = {
                    trigger: 'axis',
                    formatter: function (params) {
                        var index = params[0].dataIndex
                        var str = params[0].name + "\ndbsize(MB) : " + params[0].value + " MB\n" + data['diskinfo'][index];
                        return "<pre>" + str.replace(/\.\/experiment\/terark_wiki/g, "") + "</pre>"
                    }
                }
                dbsizeOption.series = [{
                    name: 'dbsize(MB)',
                    type: 'line',
                    data: data['dbsize']
                }]
                dbsizeChart.setOption(dbsizeOption)
            }
        })
    }

    makeRequest()
    // 间隔是一小时的时候，定期进行采样
//        if (duration == 1) {
//            setInterval(makeRequest, duration > 24 ? 100000 : 10000)
//        }
}
